function ObjectTesterInterface() {

    /**
     * 
     * @returns string containing first declaration line
     */
    var definedConstructor = function(object) {

        let classRegEx = "(function|class)\\s+" + object.constructor.name + "\\s*\\([\=\",\'a-zA-Z\\d\\s]*\\)\\s*\{";

        return object.constructor.toString().match(classRegEx)[0];
    }

    /**
     * 
     * @param {*} getDefaults on true return defaults values.
     * @returns string
     */
    this.getParams = function(object, getDefaults = true) {

        if (this.isFunction(object)) {

            let aParameters = definedConstructor(object).match("\\s*\\([\=\",\'a-zA-Z\\d\\s]*\\)\\s*\{")[0].replaceAll(/[{ \(\)\s]/g, "");
            if (getDefaults) return aParameters;

            return aParameters.replaceAll(/\=[\"\']*[a-zA-Z\_\d+]*[\"\']*/g, "");
        }

        else if (this.isClass(object)) {
            throw new Error("Sorry, ObjectStringify doesn't support class for the moment.");
        }
    }

    /**
     * 
     * @returns true if object is declared by a function
     */
    this.isFunction = function(object) {
        if (definedConstructor(object).match("function")) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @returns true if object is declared by a class
     */
    this.isClass = function(object) {
        if (definedConstructor(object).match("class")) {
            return true;
        }
        return false;
    }

    /**
     * Get an object stringify from an object declared by a function.
     * Check automatically if attribute is private, public.
     * @param {object} object
     * @param {boolean} safe_mode (with Romain)
     */
    this.stringFromFunction = function(object, safe_mode = false) {

        if (!this.isFunction(object)) {
            
            if (definedConstructor(object).match("class")) throw new Error("Object is declaring by class.\nYou should use fromClass() function.");
            throw new Error("An error occured. The constructor is perhaps not declared with any class or function");
        }

       let aParameters = this.getParams(object, false);

        if (safe_mode === false) {
            // If function has no parameters
            if (aParameters.length === 0)
                return object.constructor.name + " {}";
            else
                aParameters = aParameters.split(",");


            // Start stringification
            let stack = object.constructor.name + " (" + this.getParams(object) + ") {\n";
            let validates = [];

            for(let i in aParameters) {

                stack += "\n\t";

                // If public this.param = param
                if (object.constructor.toString().match("this." + aParameters[i] + " *\= *" + aParameters[i])){

                    let value = eval("object." + aParameters[i]);

                    stack += aParameters[i] + ": ";
                    stack += value + " (" +  typeof(value) + ")";

                    validates.push(aParameters[i]);
                }

                // If private with getter this.getParam = function() { return ...
                else if (object.constructor.toString().match(
                    "this.get" + aParameters[i].replace(aParameters[i][0], aParameters[i][0].toUpperCase()) + " *\= *function *\\(\\) *\{"
                )) {

                    let value = eval("object.get" + aParameters[i].replace(aParameters[i][0], aParameters[i][0].toUpperCase()) + "()");
                    
                    stack += "#" + aParameters[i] + ": ";
                    stack += value + " (" +  typeof(value) + ")";
                    validates.push("get" + aParameters[i].replace(aParameters[i][0], aParameters[i][0].toUpperCase()));
                }

                else if (object.constructor.toString().match(
                    "this." + aParameters[i] + " *\= *function *\\(\\) *\{"
                )) {

                    let value = eval("object." + aParameters[i] + "()");
                    
                    stack += "#" + aParameters[i] + ": ";
                    stack += value + " (" +  typeof(value) + ")";
                    validates.push(aParameters[i]);
                }

                else {
                    stack += "_" + aParameters[i] + " (unreachable)";
                }
            }

            let objKeys = Object.keys(object);

            for (let i in objKeys) {
                
                if (!validates.includes(objKeys[i]) && objKeys[i].slice(0, 3) !== "set" && objKeys[i] !== "toString") {

                    if (object.constructor.toString().match("this." + objKeys[i] + "\\s*=\\s*(function\\(\\))")) {
                        let value = eval("object." + objKeys[i] + "()");
                        stack += "\n\t()" + objKeys[i] + ": ";
                        stack += value + " (" + typeof(value) + ")";
                    }
                }
            }

            stack += "\n}";
            return stack;
        }

        else if (safe_mode === true) {

            var tabKeys = Object.keys(object);

            var tabKeysClean=[];
        
            var cpt=0;
            for (let i=0;i<tabKeys.length;i++){
                var temp=tabKeys[i];
                if (temp.charAt(0)==="g" && temp.charAt(1)==="e" && temp.charAt(2)==="t") {
                    temp=temp.slice(3);
                    tabKeysClean[cpt]=temp;
                    cpt++;
                } 
            } 
            var msg= object.constructor.name + " {";
            for (let i=0;i<tabKeysClean.length;i++){
                msg += "\n\t"+tabKeysClean[i]+" : "+eval("object.get"+tabKeysClean[i]+"()");
            }
            return msg + "\n}";
        }

    }

    /**
     * NOT AVAILABLE
     * Get an object stringify from an object declared by a class
     * 
     */
    this.stringFromClass = function(object) {
        throw new Error("Sorry, ObjectStringify doesn't support class for the moment.");

        // if (!this.isClass())
    }

    this.testFromFunction = function(safe_mode = false, objectName, ...attributes) {

        let err_mode = "message";

        try {

            if (objectName == null) {
                err_mode = "stack";
                throw new Error("You must pass an object name in parameter 2.")

            }

            console.log(this.stringFromFunction(new objectName(...attributes), safe_mode));
    
        }catch(e) {
            
            if (err_mode === "message") console.error(e.message);
            else if (err_mode === "stack") console.error(e.stack);
            
        }
    }
}