function CookieInterface() {

    /**
     * 
     * @param {string | null} key 
     * @returns {string} value
     */
    this.getCookie = function(key = null) {

        let cookies = document.cookie;
        if (cookies == "") return false;

        cookies = cookies.split(";");
        let cookiesObject = {};

        if (key == null) return cookiesObject;

        for (let cookie in cookies) {

            cookies[cookie] = cookies[cookie].split("=");
            cookiesObject[cookies[cookie][0].trim()] = cookies[cookie][1].trim();
        }

        
        if ("0123456789".includes(cookiesObject[key])) return parseInt(cookiesObject[key]);

        return cookiesObject[key];
    }

    /**
     * 
     * @param {string | null} key 
     */
    this.clearCookie = function(key = null) {

        let cookies = getCookie();

        if (key == null) {
            
            for (let cookie in cookies) {
                document.cookie = cookie + "=; " + "expires=Thu, 01 Jan 1970 00:00:00 GMT; SameSite=strict; Secure";
            }
        }

        else if (cookies[key]) {
            document.cookie = key + "=; " + "expires=Thu, 01 Jan 1970 00:00:00 GMT; SameSite=strict; Secure";
        }
        
    }

    /**
     * 
     * @param {string} key 
     * @param {string} value 
     */
    this.setCookie = function(key = null, value = null) {

        if ("0123456789".includes(value)) value = parseInt(value);
        document.cookie = key + "=" + value + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; SameSite=strict; Secure";
    }
}