/* Adding an event listener to the cart-toggler icon. */
(function () {


    /**
     * toggler icon
     */
    let cartToggler = document.getElementById("cart-toggler");
    let cartBody = document.getElementById("cart-body");
    let cartNotification = document.getElementById("cart-notification");

    cartToggler.addEventListener("click", function (e) {

        if (cartBody.classList.contains("active")) {

            cartToggler.classList.remove("active");
            cartBody.classList.remove("active");

        } else {

            cartBody.classList.add("active");
            cartToggler.classList.add("active");
        }
    });


    let panier = new Cart();

    let totalPrice = document.getElementById("cart-total-price");
    let modalPanier = document.getElementById("modal");
    let divPrix = document.getElementById("prixModal");
    let detailedCart = document.getElementById("detailed-cart");

    totalPrice.innerText = localStorage.getItem("panier-prix") + " €";
    modalPanier.insertBefore(panier.createModalHTML(), divPrix);
    let modalList = document.getElementById("cart-list-container");

    if (detailedCart) {
        detailedCart.appendChild(panier.createTableHTML());
    }

    document.body.addEventListener("click", function (e) {

        if (e.target && e.target.matches(".addPanier")) {

            let id = e.target.id.split("-")[1];

            panier.addItem(id, albums.get(id));
            updateHTML();
            notification();

        } else if (e.target && e.target.parentElement.matches(".addPanier")) {
            let id = e.target.parentElement.id.split("-")[1];

            panier.addItem(id, albums.get(id));
            updateHTML();
            notification();
        }

        if (e.target && e.target.matches(".removePanier")) {
            let id = e.target.id.split("-")[1];

            panier.removeItem(id);
            updateHTML();
            notification();
        }

        if (e.target && e.target.matches(".deletePanier")) {
            let id = e.target.id.split("-")[1];

            panier.deleteItem(id);
            updateHTML();
            notification();
        }

    });

    function updateHTML() {
        totalPrice.innerText = localStorage.getItem("panier-prix") + " €";

        modalList.remove();
        modalPanier.insertBefore(panier.createModalHTML(), divPrix);
        modalList = document.getElementById("cart-list-container");

        if (detailedCart) {
            detailedCart.innerHTML = "";
            detailedCart.appendChild(panier.createTableHTML());
        }
    }

    function notification() {

        cartNotification.classList.remove("active");
        void cartNotification.offsetWidth;

        if (Object.keys(panier.getItems()).length > 0) {
            cartNotification.classList.add("active");
        } else {
            cartNotification.classList.remove("active");
            void cartNotification.offsetWidth;
        }
    }

    notification();
})()