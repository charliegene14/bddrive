(function() {

    var listSeries      = document.getElementById("menu-list-series");
    var listAuthors     = document.getElementById("menu-list-authors");
    var currentSerie    = document.getElementById("choose-serie");
    var currentAuthor   = document.getElementById("choose-author");

    var searchBar       = document.getElementById("searchbar");

    // Obtenir le filtre auteur en paramètre URL (ID)
    var idAuthor    = parseInt(new URLSearchParams(window.location.search).get("author"));

    // Obtenir le filtre série en paramètre URL (ID)
    var idSerie     = parseInt(new URLSearchParams(window.location.search).get("serie"));

    // Mettre à jour le menu avec la série ou l'auteur en cours
    if (idSerie)    currentSerie.innerText = series.get(idSerie.toString()).nom;
    if (idAuthor)   currentAuthor.innerText = auteurs.get(idAuthor.toString()).nom;

    // Insert first serie select
    let li = document.createElement("li");
    let liContent = "<a class='dropdown-item' href='index.htm";

    if (idAuthor)   liContent += "?author=" + idAuthor;
    
    liContent += "'>choisir une série</a></li>";
    li.innerHTML = liContent;
    listSeries.appendChild(li);

    // Insert first author select
    let li2 = document.createElement("li");
    let liContent2 = "<a class='dropdown-item' href='index.htm";

    if (idSerie)   liContent2 += "?serie=" + idSerie;
    
    liContent2 += "'>choisir un auteur</a></li>";
    li2.innerHTML = liContent2;
    listAuthors.appendChild(li2);

    // Insert serie list

    var sortedSeries = new Map(
        Array.from(series).sort(function(a, b) {
            if (a[1].nom.toLowerCase() > b[1].nom.toLowerCase()) {
                return 1;
            } else {
                return -1;
            }
        })
    )

    for (let serie of sortedSeries) {

        let li = document.createElement("li");
        let liContent = "<a class='dropdown-item' href='index.htm";

        if (idAuthor)   liContent += "?author=" + idAuthor + "&";
        else            liContent += "?"

        liContent += "serie=" + serie[0] + "'>";
        liContent += serie[1].nom + "</a></li>";

        li.innerHTML = liContent;
        listSeries.appendChild(li);
    }

    // Insert author list

    var sortedAuthors = new Map(
        Array.from(auteurs).sort(function(a, b) {
            if (a[1].nom.toLowerCase() > b[1].nom.toLowerCase()) {
                return 1;
            } else {
                return -1;
            }
        })
    )

    for (let author of sortedAuthors) {

        let li = document.createElement("li");
        let liContent = "<a class='dropdown-item' href='index.htm";

        if (idSerie)   liContent += "?serie=" + idSerie + "&";
        else           liContent += "?"

        liContent += "author=" + author[0] + "'>";
        liContent += author[1].nom + "</a></li>";

        li.innerHTML = liContent;
        listAuthors.appendChild(li);
    }

    // Search module
    searchBar.querySelector("input").addEventListener("keyup", function(e) {
     
        let list = new SearchedList(this.value, albums, auteurs, series);
        searchBar.querySelector("#search-albums").innerHTML = "";
        searchBar.querySelector("#search-series").innerHTML = "";
        searchBar.querySelector("#search-authors").innerHTML = "";

        if (list.get().size != 0 && this.value.length != 0) {
            searchBar.querySelector("#search-autocompletion").setAttribute("style", "display: block");

            // Albums autocompletion
            if (list.getAlbums().size != 0) {
                searchBar.querySelector("#search-albums").setAttribute("style", "display: block");
                searchBar.querySelector("#search-albums").innerHTML = "<span style='font-weight: bold; position: relative; left: -1rem'>Albums:</span>"

                let sortedSearchAlbums = new Map(
                    Array.from(list.getAlbums()).sort(function(a, b) {
                        if (a[1].titre.toLowerCase() > b[1].titre.toLowerCase()) {
                            return 1;
                        } else {
                            return -1;
                        }
                    })
                )

                for (let album of sortedSearchAlbums) {
                
                    let li = document.createElement("li");
                    li.style.listStyle = "none";

                    liContent = "<a href='product.htm?id=" + album[0] + "'>";
                    liContent += album[1].titre;
                    liContent += "</a>";

                    li.innerHTML = liContent;
                    searchBar.querySelector("#search-albums").appendChild(li);
                }

            } else {

                searchBar.querySelector("#search-albums").setAttribute("style", "display: none");
            }

            // Series autocompletion
            if (list.getSeries().size != 0) {
                searchBar.querySelector("#search-series").setAttribute("style", "display: block");
                searchBar.querySelector("#search-series").innerHTML = "<span style='font-weight: bold; position: relative; left: -1rem'>Séries:</span>";

                let sortedSearchSeries = new Map(
                    Array.from(list.getSeries()).sort(function(a, b) {
                        if (a[1].nom.toLowerCase() > b[1].nom.toLowerCase()) {
                            return 1;
                        } else {
                            return -1;
                        }
                    })
                )

                for (let serie of sortedSearchSeries) {
                
                    let li = document.createElement("li");
                    li.style.listStyle = "none";

                    liContent = "<a href='index.htm?serie=" + serie[0] + "'>";
                    liContent += serie[1].nom;
                    liContent += "</a>";

                    li.innerHTML = liContent;
                    searchBar.querySelector("#search-series").appendChild(li);
                }

            } else {

                searchBar.querySelector("#search-series").setAttribute("style", "display: none");
            }

            // Authors autocompletion
            if (list.getAuthors().size !=0) {
                searchBar.querySelector("#search-authors").setAttribute("style", "display: block");
                searchBar.querySelector("#search-authors").innerHTML = "<span style='font-weight: bold; position: relative; left: -1rem'>Auteurs:</span>";

                let sortedSearchAuthors = new Map(
                    Array.from(list.getAuthors()).sort(function(a, b) {
                        if (a[1].nom.toLowerCase() > b[1].nom.toLowerCase()) {
                            return 1;
                        } else {
                            return -1;
                        }
                    })
                )

                for (let author of sortedSearchAuthors) {
                
                    let li = document.createElement("li");
                    li.style.listStyle = "none";

                    liContent = "<a href='index.htm?author=" + author[0] + "'>";
                    liContent += author[1].nom;
                    liContent += "</a>";

                    li.innerHTML = liContent;
                    searchBar.querySelector("#search-authors").appendChild(li);
                }
            } else {

                searchBar.querySelector("#search-authors").setAttribute("style", "display: none");
            }

        } else {
            searchBar.querySelector("#search-autocompletion").setAttribute("style", "display: none");
        }
    })

    searchBar.addEventListener("submit", function(e) {
        e.preventDefault();
        window.location.href = "index.htm?search=" + e.target.querySelector("input").value;
    })

})()