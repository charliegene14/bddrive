(function() {

    var notFound            = document.getElementById("bd-not-found");
    var productContainer    = document.getElementById("product-container");

    // Elements HTML à mettre à jour
    var pageTitle           = document.querySelector("#page-title");
    var productImg          = document.querySelectorAll(".product-picture");
    var productAuthor       = document.getElementById("infos-author");
    var productSerie        = document.getElementById("infos-serie");
    var productNumber       = document.getElementById("infos-number");
    var productPrice        = document.getElementById("infos-price");
    var productSummary      = document.getElementById("product-summary");
    
    var customSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    var id          = new URLSearchParams(window.location.search).get("id");
    var album       = albums.get(id);

    // Contrôles
    if  (   !id
            || isNaN(id)
            || !album
        )
    {
        pageTitle.remove();
        productContainer.remove();
        notFound.style.display = "block";
        return;  
    }

    // Récup données liées
    var serie   = series.get(album.idSerie);
    var author  = auteurs.get(album.idAuteur);

    // Construction de l'item (personnalisation potentielle des données)
    var item = new ListItem(
        id,
        album.titre,
        serie.nom,
        author.nom,
        album.numero,
        album.prix,
        customSummary
    );

    // Insérer les éléments
    pageTitle.querySelector("h2").innerText = item.getTitle();

    for (let img of productImg) {
        img.src = "assets/img/" + item.getImg() + ".jpg";
        img.alt = album.titre;
    }

    productAuthor.innerText = item.getAuthor();
    productSerie.innerText = item.getSerie();
    productNumber.innerText = item.getNumber();
    productPrice.innerText = item.getPrice() + "€";
    productSummary.innerText = item.getSummary();
    document.querySelector(".addPanier.bdId").id = "bd-" + id;

    // Insérer des suggestions ()
    

})()