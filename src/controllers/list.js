(function () {

    var formSort = document.getElementById("form-sort");
    var pageTitle = document.querySelector("#page-title h2");
    var list;

    var customSummary = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    // Défini le nombre de BD par page
    const ITEMS_BY_PAGE = 20;

    // Obtenir la page en paramètre URL
    var page = parseInt(new URLSearchParams(window.location.search).get("page"));
    if (isNaN(page)) page = 0;

    // Obtenir le filtre auteur en paramètre URL (ID)
    var idAuthor = parseInt(new URLSearchParams(window.location.search).get("author"));
    if (isNaN(idAuthor)) idAuthor = 0;

    // Obtenir le filtre série en paramètre URL (ID)
    var idSerie = parseInt(new URLSearchParams(window.location.search).get("serie"));
    if (isNaN(idSerie)) idSerie = 0;

    // Obtenir le tri choisi en paramètre URL
    var sort = new URLSearchParams(window.location.search).get("sort");
    if (sort !== "idAsc"
        && sort !== "idDesc"
        && sort !== "priceDesc"
        && sort !== "priceAsc"
        && sort !== "authorDesc"
        && sort !== "authorAsc"
        && sort !== "serieDesc"
        && sort !== "serieAsc"
    )
        sort = "idDesc";

    // Obtenir la recherche en paramètre URL
    var search = new URLSearchParams(window.location.search).get("search");

    if (search || search == 0) {
        list = new SearchedList(search, albums, auteurs, series).get();
    } else {
        // Filtrer la liste des albums selon les filtres
        list = new FilteredList(albums, idAuthor, idSerie).get();
    }

    // Trier la liste selon le tri choisi
    list = new SortedList(list, auteurs, series, sort).get();

    //Insérer le nombre de résultats au DOM
    document.getElementById("result-number").innerHTML = "(" + list.size + ")";

    // Obtenir la liste de la page actuelle et l'insérer au DOM
    let paginatedList = new PaginatedList(list, ITEMS_BY_PAGE, page)

    list = paginatedList.get();

    for (album of list) {

        let id = parseInt(album[0]);
        let title = album[1].titre;
        let serie = series.get(album[1].idSerie).nom;
        let author = auteurs.get(album[1].idAuteur).nom
        let number = album[1].numero;
        let price = parseFloat(album[1].prix);

        let item = new ListItem(id, title, serie, author, number, price, customSummary);
        document.getElementById("list-container").appendChild(item.dom());
    }

    // Définir la taille de la pagination (nombre de pages)
    let indexing;

    if (window.innerWidth < 360) indexing = 3;
    else if (window.innerWidth < 480) indexing = 5;
    else if (window.innerWidth < 640) indexing = 7;
    else if (window.innerWidth < 800) indexing = 9;
    else indexing = 11;

    // Créer la pagination et l'insérer au DOM

    let pagination = new Pagination(
        paginatedList.getTotalPages(),
        indexing,
        page,
        idAuthor,
        idSerie,
        sort,
        search
    );

    document.getElementById("list-container").appendChild(pagination.dom());

    // Mettre à jour le titre de page
    if (idAuthor) pageTitle.innerText = auteurs.get(idAuthor.toString()).nom;
    if (idSerie) pageTitle.innerText = series.get(idSerie.toString()).nom;

    if (idAuthor && idSerie) pageTitle.innerText = series.get(idSerie.toString()).nom + " - " + auteurs.get(idAuthor.toString()).nom;
    if (search || search == 0) pageTitle.innerText = "Recherche: " + search;

    // Select form

    let optionsSort = document.getElementsByClassName("sort-option");

    for (let option of optionsSort) {
        if (option.value == sort) {
            option.setAttribute("selected", "selected");
        }
    }

    formSort.addEventListener("change", function (e) {
        e.preventDefault();
        let url = "?";

        url += "page=1&";
        if (idAuthor) url += "author=" + idAuthor + "&";
        if (idSerie) url += "serie=" + idSerie + "&";
        if (search) url += "search=" + search + "&";

        url += "sort=" + e.target.value;
        window.location.href = url;
    })

    // Reset button
    if (idAuthor || idSerie || search || search == 0) {

        // <a style="width: 16px; height: 16px;" class="btn btn-primary m-1 p-0" href="index.htm"><i style="font-size: 12px; vertical-align: super;" class="fas fa-times m-0 p-0"></i></a>
        let a = document.createElement("a");
        a.setAttribute("style", "width: 16px; height: 16px;");
        a.setAttribute("class", "btn btn-primary m-1 p-0");
        a.href = "index.htm";

        a.innerHTML = "<i style='font-size: 12px; vertical-align: super;' class='fas fa-times m-0 p-0'></i>";

        document.getElementById("result-number").appendChild(a);
    }

})()