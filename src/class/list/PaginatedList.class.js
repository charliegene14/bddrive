/**
 * 
 * @param {Map} list 
 * @param {number} itemsByPage 
 * @param {number} currentPage 
 */
function PaginatedList(list, itemsByPage = 10, currentPage = 1) {

    var myList;
    var myItemsByPage;
    var myCurrentPage;

    var paginatedList;
    var totalPages;

    /**
     * 
     * @returns {Map}
     */
    this.getList = function () { return myList; }

    /**
     * 
     * @returns {number}
     */
    this.getItemsByPage = function () { return parseInt(myItemsByPage); }

    /**
     * 
     * @returns {number}
     */
    this.getCurrentPage = function () { return parseInt(myCurrentPage); }

    /**
     * 
     * @returns {number}
     */
    this.getTotalPages = function () { return parseInt(totalPages); }

    /**
     * 
     * @returns {Map}
     */
    this.get = function () { return paginatedList; }

    /**
     * 
     * @param {Map} newList
     */
    var setList = function (newList) {

        if (!newList instanceof Map) throw new Error("List must be a Map object");
        myList = newList;
    }

    /**
     * 
     * @param {number} newItemsByPage 
     */
    var setItemsByPage = function (newItemsByPage) {

        if (!newItemsByPage || isNaN(newItemsByPage)) throw new Error("Items by page is incorrect.");
        myItemsByPage = parseInt(newItemsByPage);
    }

    /**
     * 
     * @param {number} newListSize
     * @param {number} newItemsByPage 
     */
    var setTotalPages = function (newListSize, newItemsByPage) {

        if (newListSize == 0) {
            totalPages = 0;
            return;
        }
        if (!newListSize || isNaN(newListSize)) throw new Error("List size is incorrect.");
        if (!newItemsByPage || isNaN(newItemsByPage)) throw new Error("Items by page is incorrect.");

        totalPages = Math.ceil(newListSize / newItemsByPage);
    }

    /**
     * 
     * @param {number} newCurrentPage 
     */
    var setCurrentPage = function (newCurrentPage) {

        if (isNaN(newCurrentPage)) {
            throw new Error("Current page is incorrect.");
        }

        if (newCurrentPage <= 0) newCurrentPage = 1;
        if (newCurrentPage > totalPages) newCurrentPage = totalPages;

        myCurrentPage = parseInt(newCurrentPage);
    }

    /**
     * 
     * @param {Map} newList 
     * @param {number} newItemsByPage 
     * @param {number} newCurrentPage 
     */
    var setPaginatedList = function (newList, newItemsByPage, newCurrentPage) {

        if (!newList instanceof Map) throw new Error("List must be a Map object");

        if (!newItemsByPage || isNaN(newItemsByPage)) throw new Error("Items by page is incorrect.");
        if ((!newCurrentPage && !newCurrentPage == 0)
            || isNaN(newCurrentPage)
            || newCurrentPage < 0
            || newCurrentPage > totalPages
        ) {
            throw new Error("Current page is incorrect.");
        }

        let firstPost = (newCurrentPage - 1) * newItemsByPage;

        paginatedList = new Map(
            Array
                .from(newList)
                .slice(firstPost, firstPost + newItemsByPage)
        );
    }


    // Init
    setList(list);
    setItemsByPage(itemsByPage);
    setTotalPages(this.getList().size, this.getItemsByPage());
    setCurrentPage(currentPage);
    setPaginatedList(this.getList(), this.getItemsByPage(), this.getCurrentPage())
}