/**
 * 
 * @param {Map} list
 * @param {Map} authorList
 * @param {Map} serieList
 * @param {string} param
 * 
 */
function SortedList(list, authorList, serieList, param = "idDesc") {

    const ID_ASC        = "idAsc";
    const ID_DESC       = "idDesc";
    const PRICE_ASC     = "priceAsc";
    const PRICE_DESC    = "priceDesc";
    const AUTHOR_ASC    = "authorAsc";
    const AUTHOR_DESC   = "authorDesc";
    const SERIE_ASC     = "serieAsc";
    const SERIE_DESC    = "serieDesc"

    var myList;
    var myParam;
 
    var sortedList;

    /**
     * 
     * @returns {Map}
     */
    this.getList = function() { return myList; }

    /**
     * 
     * @returns {string}
     */
    this.getParam = function() { return myParam; }

    this.get = function() { return sortedList; }

    /**
     * 
     * @param {Map} newList
     */
    var setList = function(newList) {

        if (!newList instanceof Map) throw new Error("List must be a Map object");
        myList = newList;
    }

    /**
     * 
     * @param {string} newParam 
     */
    var setParam = function(newParam = ID_DESC) {

        if (newParam && typeof(newParam) !== "string") throw new Error("Param is incorrect.");

        if (!newParam) myParam = ID_DESC;
        else myParam = newParam;
    }

    /**
     * 
     * @param {Map} newList 
     * @param {string} newParam 
     */
    var setSortedList = function(newList, newParam = ID_DESC) {

        if (!newList instanceof Map) throw new Error("List must be a Map object");

        if (newParam === ID_ASC) {
            sortedList = newList;
        }

        else if (newParam === ID_DESC) {

            sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {
                        return b[0] - a[0]
                    })
            );
        }

        else if (newParam === PRICE_DESC) {

            sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {
                        return b[1].prix - a[1].prix;
                    })
            );
        }

        else if (newParam === PRICE_ASC) {
             sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {
                        return a[1].prix - b[1].prix;
                    })
            );
        }

        else if (newParam === AUTHOR_ASC) {
            
            sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {

                        let authorAName = authorList.get(a[1].idAuteur).nom.toLowerCase();
                        let authorBName = authorList.get(b[1].idAuteur).nom.toLowerCase();

                        if (authorAName > authorBName) {
                            return 1;
                        } else {
                            return -1;
                        }
    
                    })
            );
        }

        else if (newParam === AUTHOR_DESC) {
            sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {

                        let authorAName = authorList.get(a[1].idAuteur).nom.toLowerCase();
                        let authorBName = authorList.get(b[1].idAuteur).nom.toLowerCase();

                        if (authorAName > authorBName) {
                            return -1;
                        } else {
                            return 1;
                        }

                    })
            );
            
        }

        else if (newParam === SERIE_ASC) {

            sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {

                        let serieAName = serieList.get(a[1].idSerie).nom.toLowerCase();
                        let serieBName = serieList.get(b[1].idSerie).nom.toLowerCase();

                        if (serieAName > serieBName) {
                            return 1;
                        } else {
                            return -1;
                        }
    
                    })
            );
        }

        else if (newParam === SERIE_DESC) {
            sortedList = new Map(
                Array
                .from(newList)
                .sort(function(a, b) {

                        let serieAName = serieList.get(a[1].idSerie).nom.toLowerCase();
                        let serieBName = serieList.get(b[1].idSerie).nom.toLowerCase();

                        if (serieAName > serieBName) {
                            return -1;
                        } else {
                            return 1;
                        }
    
                    })
            );
            
        }


        else {
            
            throw new Error("Param is incorrect.")
        }
    }

    // Init
    setList(list);
    setParam(param);
    setSortedList(this.getList(), this.getParam());
}