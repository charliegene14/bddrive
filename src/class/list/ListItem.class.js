/**
 * 
 * @param {number} id 
 * @param {string} title 
 * @param {string} serie serie name
 *  @param {string} author serie name
 * @param {string} number album number in the serie
 * @param {number} price
 * @param {string} summary
 */
function ListItem(id, title, serie, author, number, price, summary) {

    // Constructor
    var myId;
    var myTitle;
    var mySerie;
    var myAuthor;
    var myNumber;
    var myPrice;
    var mySummary;
    var myImg;

    const IMG_FOLDER = "assets/img/";
    const IMG_FORMAT = "jpg";
    const CSS_ITEM_NAME = "list-item";
    const ADD_TO_CART = "addPanier";
    
    // Getters

    /**
     * 
     * @returns {number}
     */
    this.getId = function() { return parseInt(myId); }

    /**
     * 
     * @returns {string}
     */
    this.getTitle = function() { return myTitle; }

    /**
     * 
     * @returns {string}
     */
    this.getSerie = function() { return mySerie; }

    /**
     * 
     * @returns {string}
     */
    this.getAuthor = function() { return myAuthor; }

    /**
     * 
     * @returns {string}
     */
    this.getNumber = function() { return myNumber; }

    /**
     * 
     * @returns {number}
     */
    this.getPrice = function() { return parseFloat(myPrice); }

    /**
     * 
     * @returns {string}
     */
    this.getImg = function() { return myImg; }

    /**
     * 
     * @returns {string}
     */
    this.getSummary = function() { return mySummary; }

    // Setters

    /**
     * 
     * @param {number} newId 
     */
    var setId = function(newId) {

        if (!newId || isNaN(newId)) throw new Error("ID is incorrect.");
        myId = parseInt(newId);
    }

    /**
     * 
     * @param {string} newTitle 
     */
    var setTitle = function(newTitle) {
        if (!newTitle || typeof(newTitle) !== "string") throw new Error("Title is incorrect.");
        myTitle = newTitle;
    }

    /**
     * 
     * @param {string} newSerie 
     */
    var setSerie = function(newSerie) {
        if (!newSerie || typeof(newSerie) !== "string") throw new Error("Serie is incorrect.");
        mySerie = newSerie;
    }

    var setAuthor = function(newAuthor) {
        if (!newAuthor || typeof(newAuthor) !== "string") throw new Error("Author is incorrect.");
        myAuthor = newAuthor;
    }

    /**
     * 
     * @param {string} newNumber 
     */
    var setNumber = function(newNumber) {
        if (!newNumber) throw new Error("Number is incorrect.");
        myNumber = newNumber;
    }

    /**
     * 
     * @param {number} newPrice 
     */
    var setPrice = function(newPrice) {
        if (!newPrice || isNaN(newPrice)) throw new Error("Price is incorrect.");
        myPrice = parseFloat(newPrice);
    }

    /**
     * 
     * @param {string} newSerie 
     * @param {string} newNumber 
     * @param {string} newTitle 
     */
    var setImg = function(newSerie, newNumber, newTitle) {

        if (!newSerie || typeof(newSerie) !== "string") throw new Error("Serie is incorrect.");
        if (!newNumber) throw new Error("Number is incorrect.");
        if (!newTitle || typeof(newTitle) !== "string") throw new Error("Title is incorrect.");

        let img = newSerie + "-" + newNumber + "-" + newTitle;
        let regex = /['$.!?:]/gi;

        myImg = img.replaceAll(regex, "");
    }

    /**
     * 
     * @param {string} newSummary 
     */
    var setSummary = function(newSummary) {
        if (typeof(newSummary) !== "string") throw new Error("Summary is incorrect.");
        mySummary = newSummary;
    }

    /**
     * 
     * @returns {HTMLElement} The DIV element (class HTMLElement)
     */
    this.dom = function() {

        let cardAlbum = document.createElement("div");
        cardAlbum.classList.add("card");
        cardAlbum.classList.add(CSS_ITEM_NAME);

        let cardAlbumContent    =   "<img src='" + IMG_FOLDER + myImg + "." + IMG_FORMAT + "' class='card-img-top' alt='" + myImg + "'>";
        cardAlbumContent        +=  "<div class='card-body'>";
        cardAlbumContent        +=  "<h1 class='card-title'>" + myTitle + "</h1><h2>" + mySerie + "<br />" + myAuthor + "</h2>";
        cardAlbumContent        +=  "<p class='card-text'><span class='card-price'>" + myPrice + "€</span></p>";
        cardAlbumContent        +=  "<a href='product.htm?id=" + myId + "' class='btn btn-primary'>Details</a>";
        cardAlbumContent        +=  "<i class='fas fa-cart-plus " + ADD_TO_CART + "' id=bd-" + myId + "></i></div>";

        cardAlbum.innerHTML = cardAlbumContent;

        return cardAlbum;
    }

    // Init
    setId(id);
    setTitle(title);
    setSerie(serie);
    setAuthor(author);
    setNumber(number);
    setPrice(price);
    setImg(serie, number, title);
    setSummary(summary);
}