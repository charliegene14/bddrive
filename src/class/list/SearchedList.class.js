/**
 * 
 * @param {string} search 
 * @param {Map} albums 
 * @param {Map} auteurs 
 * @param {Map} series 
 */
function SearchedList(search, albums, authors, series) {

    var resultAuthors = new Map();
    var resultSeries = new Map();
    var resultAlbums = new Map();

    var results = new Map();

    search = search.toLowerCase();

    /**
     * 
     * @returns {Map}
     */
    this.get = function() { return results; }

    /**
     * 
     * @returns {Map}
     */
    this.getAuthors = function() { return resultAuthors; }

    /**
     * 
     * @returns {Map}
     */
    this.getSeries = function() { return resultSeries; }

    /**
     * 
     * @returns {Map}
     */
    this.getAlbums = function() { return resultAlbums; }

    var searchInAuthors = function() {
        
        for (let author of authors) {

            if (author[1].nom.toLowerCase().includes(search)) {

                resultAuthors.set(author[0], author[1]);

                for (let album of albums) {
                    if (album[1].idAuteur == author[0]) {
                        
                        results.set(album[0], album[1]);
                    }
                }
            }
        }
    }

    var searchInSeries = function() {
        for (let serie of series) {
            if (serie[1].nom.toLowerCase().includes(search)) {

                resultSeries.set(serie[0], serie[1]);
                
                for (let album of albums) {
                    if (album[1].idSerie == serie[0]) {
    
                        results.set(album[0], album[1]);
                    }
                }
            }
        }
    }

    var searchInAlbums = function() {
   
        for (let album of albums) {

           if (album[1].titre.toLowerCase().includes(search)) {
               resultAlbums.set(album[0], album[1]);
               results.set(album[0], album[1]);
           }
        }

    }

    // init
    searchInAuthors();
    searchInSeries();
    searchInAlbums();
}