/**
 * 
 * @param {Map} list 
 * @param {number} author 
 * @param {number} serie 
 */

function FilteredList(list, idAuthor = 0, idSerie = 0) {

    // Constructor
    var myList;
    var myIdAuthor;
    var myIdSerie;

    // Private attribute
    var filteredList;

    // Getters

    /**
     * 
     * @returns {Map}
     */
    this.getList = function () { return myList; }

    /**
     * 
     * @returns {number}
     */
    this.getIdAuthor = function () { return parseInt(myIdAuthor); }

    /**
     * 
     * @returns {number}
     */
    this.getIdSerie = function () { return parseInt(myIdSerie); }

    /**
     * 
     * @returns {Map}
     */
    this.get = function () { return filteredList; }

    // Setters

    /**
     * 
     * @param {Map} newList
     */
    var setList = function (newList) {

        if (!newList instanceof Map) throw new Error("List must be a Map object");
        myList = newList;
    }

    /**
     * 
     * @param {number} newIdAuthor 
     */
    var setIdAuthor = function (newIdAuthor = 0) {
        if (newIdAuthor && isNaN(newIdAuthor)) throw new Error("Author ID is incorrect.");
        myIdAuthor = parseInt(newIdAuthor);
    }

    /**
     * 
     * @param {number} newIdSerie 
     */
    var setIdSerie = function (newIdSerie = 0) {
        if (newIdSerie && isNaN(newIdSerie)) throw new Error("Serie ID is incorrect.");
        myIdSerie = parseInt(newIdSerie);
    }

    /**
     * Filter the list according to the parameters
     */
    var setFilteredList = function (newList, newIdAuthor = 0, newIdSerie = 0) {

        let list = new Map();

        if (!newIdAuthor && !newIdSerie) {
            list = newList;
        }

        else if (newIdAuthor && !newIdSerie) {

            for (let album of newList) {
                if (parseInt(album[1].idAuteur) === newIdAuthor) list.set(album[0], album[1]);
            }
        }

        else if (!newIdAuthor && newIdSerie) {
            for (let album of newList) {
                if (parseInt(album[1].idSerie) === newIdSerie) list.set(album[0], album[1]);
            }
        }

        else if (newIdAuthor && newIdSerie) {
            for (let album of newList) {
                if (parseInt(album[1].idSerie) === newIdSerie && parseInt(album[1].idAuteur) === newIdAuthor) list.set(album[0], album[1]);
            }
        }

        filteredList = list;
    }

    // Init
    setList(list);
    setIdAuthor(idAuthor);
    setIdSerie(idSerie);
    setFilteredList(this.getList(), this.getIdAuthor(), this.getIdSerie());
}