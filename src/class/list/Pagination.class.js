/**
 * 
 * @param {number} totalPages 
 * @param {number} indexByView 
 * @param {number} currentPage
 * @param {number} author
 * @param {number} serie
 * @param {string} sort
 * @param {string|number} search
 */

function Pagination(totalPages, indexByView = 11, currentPage = 1, author = 0, serie = 0, sort = 0, search = "") {

    var myTotal;
    var myIndex;
    var myCurrent;

    var pageStart = 1;

    /**
     * 
     * @returns {number}
     */
    this.getTotal = function () { return parseInt(myTotal); }

    /**
     * 
     * @returns {number}
     */
    this.getIndex = function () { return parseInt(myIndex); }

    /**
     * 
     * @returns {number}
     */
    this.getCurrent = function () { return parseInt(myCurrent); }

    /**
     * 
     * @param {number} newTotal 
     */
    var setTotal = function (newTotal) {
        if ((!newTotal && !newTotal == 0) || isNaN(newTotal)) throw new Error("Total page is incorrect.");
        myTotal = parseInt(newTotal);
    }

    /**
     * 
     * @param {number} newIndex 
     */
    var setIndex = function (newIndex) {

        if (!newIndex || isNaN(newIndex)) throw new Error("Indexing is incorrect.");

        if (myTotal < newIndex) {

            myIndex = parseInt(myTotal);
        } else {
            myIndex = parseInt(newIndex);
        }
    }

    /**
     * 
     * @param {number} newCurrent 
     */
    var setCurrent = function (newCurrent) {

        if (isNaN(newCurrent)) {
            throw new Error("Current page is incorrect.");
        }

        if (newCurrent <= 0) newCurrent = 1;
        if (newCurrent > myTotal) newCurrent = myTotal;

        myCurrent = parseInt(newCurrent);
    }

    /**
     * Initialize the first pagination's page
     * @var pageStart
     */
    var init = function () {

        if (myCurrent <= Math.floor(myIndex / 2) || myTotal < myIndex) pageStart = 1;

        else if (myCurrent >= myTotal - Math.floor(myIndex / 2)) pageStart = myTotal - (myIndex - 1);

        else pageStart = myCurrent - Math.floor(myIndex / 2);
    }

    /**
     * 
     * @returns {HTMLElement} The DIV element (class HTMLElement)
     */
    this.dom = function () {

        let pagination = document.createElement("nav");
        let contentPagination = "<ul class='pagination'>";

        // Page précédente
        contentPagination += myCurrent > 1 ? "<li class='page-item'>" : "<li class='page-item disabled'>";
        contentPagination += "<a class='page-link' href='?page=" + parseInt(myCurrent - 1);

        if (author) contentPagination += "&author=" + parseInt(author);
        if (serie) contentPagination += "&serie=" + parseInt(serie);
        if (sort) contentPagination += "&sort=" + sort;


        contentPagination += "' aria-label='Previous'>";
        contentPagination += "<span aria-hidden='true'>&laquo;</span>";
        contentPagination += "<span class='sr-only'>Précédent</span>";
        contentPagination += "</a></li>";

        // Retourner à la première page
        contentPagination += myTotal > myIndex && myCurrent > Math.floor(myIndex / 2) + 1 ? "<li class='page-item'>" : "<li class='page-item disabled'>";
        contentPagination += "<a class='page-link' href='?page=1";

        if (author) contentPagination += "&author=" + parseInt(author);
        if (serie) contentPagination += "&serie=" + parseInt(serie);
        if (sort) contentPagination += "&sort=" + sort;
        if (search || search == 0) contentPagination += "&search=" + search;

        contentPagination += "' aria-label='Start'>";
        contentPagination += "<span aria-hidden='true'>...</span>";
        contentPagination += "<span class='sr-only'>Début</span>";
        contentPagination += "</a></li>";

        // Afficher les pages
        for (let i = pageStart; i < pageStart + myIndex; i++) {

            if (i == myCurrent) {

                contentPagination += "<li class='page-item disabled'>";
                contentPagination += "<a class='page-link' href='#'>" + i + "</a>";

            } else {
                contentPagination += "<li class='page-item'>";
                contentPagination += "<a class='page-link' href='?page=" + i;

                if (author) contentPagination += "&author=" + parseInt(author);
                if (serie) contentPagination += "&serie=" + parseInt(serie);
                if (sort) contentPagination += "&sort=" + sort;
                if (search || search == 0) contentPagination += "&search=" + search;

                contentPagination += "'>" + i + "</a>";
            }

            contentPagination += "</li>";
        }

        // Aller à la dernière page
        contentPagination += myTotal > myIndex && myCurrent <= myTotal - Math.floor(myIndex / 2) + 1 ? "<li class='page-item'>" : "<li class='page-item disabled'>";
        contentPagination += "<a class='page-link' href='?page=" + myTotal;

        if (author) contentPagination += "&author=" + parseInt(author);
        if (serie) contentPagination += "&serie=" + parseInt(serie);
        if (sort) contentPagination += "&sort=" + sort;
        if (search || search == 0) contentPagination += "&search=" + search;

        contentPagination += "' aria-label='Start'>";
        contentPagination += "<span aria-hidden='true'>...</span>";
        contentPagination += "<span class='sr-only'>Fin</span>";
        contentPagination += "</a></li>";

        // Page suivante
        contentPagination += myCurrent < myTotal ? "<li class='page-item'>" : "<li class='page-item disabled'>";
        contentPagination += "<a class='page-link' href='?page=" + parseInt(myCurrent + 1);

        if (author) contentPagination += "&author=" + parseInt(author);
        if (serie) contentPagination += "&serie=" + parseInt(serie);
        if (sort) contentPagination += "&sort=" + sort;
        if (search || search == 0) contentPagination += "&search=" + search;

        contentPagination += "' aria-label='Next'>";
        contentPagination += "<span aria-hidden='true'>&raquo;</span>";
        contentPagination += "<span class='sr-only'>Suivant</span>";
        contentPagination += "</a></li>";

        contentPagination += " </ul>";

        pagination.innerHTML = contentPagination;
        return pagination;
    }

    // Init
    setTotal(totalPages);
    setIndex(indexByView);
    setCurrent(currentPage);

    init();
}