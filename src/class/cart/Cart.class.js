/**
 * Create a cart object that can be used to manage the items in the cart
 * @returns The cart object is being returned.
 */
function Cart() {

    var myItems;
    var myPrixTotal;

    this.getItems = function () {
        return myItems;
    };

    var setItems = function (strStorage) {

        if (!strStorage) myItems = {};
        else myItems = JSON.parse(strStorage);

    }

    /* This function is used to initialize the price total of the cart. */
    var initPrixTotal = function (strStorage) {

        if (!strStorage) {
            myPrixTotal = 0;
        }
        else {
            myPrixTotal = parseFloat(strStorage).toFixed(2);
        }

    }

    /* The function `addItem` is used to add an item to the cart. */
    this.addItem = function (newId, newItem) {
        if (!myItems[newId]) {
            myItems[newId] = newItem;
            myItems[newId].qte = 1;
            myItems[newId].prixTotal = newItem.prix;
        }
        else {
            myItems[newId].qte++;
            myItems[newId].prixTotal = (parseFloat(myItems[newId].prix) * parseInt(myItems[newId].qte)).toFixed(2);
        }

        localStorage.setItem("panier", JSON.stringify(myItems));

        calPrixTotal();
    };

    this.deleteItem = function (newId) {

        delete myItems[newId];
        localStorage.setItem("panier", JSON.stringify(myItems));
        calPrixTotal();
    };

    this.removeItem = function (newId) {

        if (myItems[newId].qte <= 1) {
            this.deleteItem(newId);
        }
        else {

            myItems[newId].qte--;
            myItems[newId].prixTotal = (parseFloat(myItems[newId].prix) * parseInt(myItems[newId].qte)).toFixed(2);
            localStorage.setItem("panier", JSON.stringify(myItems));
            calPrixTotal();
        }
    };

    this.getPrixPanier = function () {
        return myPrixTotal;
    };

    this.createTableHTML = function () {  // creation du tableau pour le panier detaillé

        let table = document.createElement("table");       // creation du tableau pour le panier
        table.classList.add("table", "table-striped");

        let colgroup = document.createElement("colgroup");
        colgroup.classList.add("columns");

        let entetePanier = document.createElement("thead");
        let trEntete = document.createElement("tr");
        let thArticle = document.createElement("th");
        let thPrix = document.createElement("th");
        let thQte = document.createElement("th");
        let thPrixTotal = document.createElement("th");

        thArticle.textContent = "Article";
        thPrix.textContent = "Prix U.";
        thQte.textContent = "Qté";
        thPrixTotal.textContent = "Prix T.";

        trEntete.appendChild(thArticle);
        trEntete.appendChild(thPrix);
        trEntete.appendChild(thQte);
        trEntete.appendChild(thPrixTotal);

        entetePanier.appendChild(trEntete);

        table.appendChild(entetePanier);
        let tCorps = document.createElement("tbody");

        for (let i in myItems) {

            let trArticle = document.createElement("tr");
            let tdTitre = document.createElement("td");
            let tdPrix = document.createElement("td");
            let tdQte = document.createElement("td");
            let tdPrixTotal = document.createElement("td");
            let moins = document.createElement("i");
            moins.setAttribute("class", "fas fa-minus removePanier fs-6");
            moins.id = "bd-" + i;
            let plus = document.createElement("i");
            plus.setAttribute("class", "fas fa-plus addPanier fs-6");
            plus.id = "bd-" + i;

            let tdPoubelle = document.createElement("td");

            let poubelle = document.createElement("i");
            poubelle.setAttribute("class", "fas fa-trash-alt deletePanier fs-6");
            poubelle.id = "bd-" + i;

            let qteSpan = document.createElement("span");

            tdTitre.textContent = myItems[i].titre;
            tdPrix.textContent = myItems[i].prix + "€";
            qteSpan.textContent = myItems[i].qte;
            tdPrixTotal.textContent = myItems[i].prixTotal + "€";

            trArticle.appendChild(tdTitre);
            trArticle.appendChild(tdPrix);
            tdQte.appendChild(moins);
            tdQte.appendChild(qteSpan);
            tdQte.appendChild(plus);
            trArticle.appendChild(tdQte);
            trArticle.appendChild(tdPrixTotal);
            tdPoubelle.appendChild(poubelle);
            trArticle.appendChild(tdPoubelle)
            // tdPrixTotal.appendChild(poubelle);

            tCorps.appendChild(trArticle);

        }
        table.appendChild(tCorps);

        let piedPage = document.createElement("tfoot");
        let trPiedPage = document.createElement("tr");
        let tdPiedPage = document.createElement("td");
        let tdPiedPagePrix = document.createElement("td");

        tdPiedPage.setAttribute("colspan", 3);
        tdPiedPagePrix.setAttribute("colspan", 2);

        tdPiedPage.textContent = "Prix total du panier : ";
        tdPiedPagePrix.textContent = localStorage.getItem("panier-prix") + "€";
        trPiedPage.appendChild(tdPiedPage);
        trPiedPage.appendChild(tdPiedPagePrix);
        piedPage.appendChild(trPiedPage);
        table.appendChild(piedPage);

        return table;
    }

    this.createModalHTML = function () {

        let ul = document.createElement("ul");
        ul.classList.add("list-group");
        ul.id = "cart-list-container";

        for (let i in myItems) {

            let li = document.createElement("li");
            li.classList.add("list-group-item");

            let itemSpan = document.createElement("span");
            itemSpan.setAttribute("class", "cart-item-name");

            let manageItem = document.createElement("div");
            manageItem.setAttribute("class", "cart-item-manage");

            let qteItem = document.createElement("div");
            qteItem.setAttribute("class", "cart-item-quantity");

            let moins = document.createElement("i");
            moins.setAttribute("class", "fas fa-minus removePanier");
            moins.id = "bd-" + i;

            let plus = document.createElement("i");
            plus.setAttribute("class", "fas fa-plus addPanier");
            plus.id = "bd-" + i;

            let spanPoubelle = document.createElement("span")
            spanPoubelle.setAttribute("class", "cart-item-delete deletePanier");
            let poubelle = document.createElement("i");
            poubelle.setAttribute("class", "fas fa-trash-alt deletePanier");
            poubelle.id = "bd-" + i;

            let qteSpan = document.createElement("span");

            itemSpan.textContent = myItems[i].titre;
            li.appendChild(itemSpan);

            qteSpan.textContent = myItems[i].qte;
            qteItem.appendChild(moins);
            qteItem.appendChild(qteSpan);
            qteItem.appendChild(plus);
            manageItem.appendChild(qteItem);
            spanPoubelle.appendChild(poubelle);
            manageItem.appendChild(spanPoubelle);
            li.appendChild(manageItem);
            ul.appendChild(li);
        }

        return ul;
    }

    var calPrixTotal = function () {
        let sum = 0;

        for (let i in myItems) {
            sum += parseFloat(myItems[i].prixTotal);
        }

        localStorage.setItem("panier-prix", sum.toFixed(2));

    };

    // init

    setItems(localStorage.getItem("panier"));
    initPrixTotal(localStorage.getItem("panier-prix"));

}